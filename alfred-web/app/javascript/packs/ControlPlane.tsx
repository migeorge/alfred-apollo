import reactMountElements from 'react-mount-elements'

import Main from '../control-plane/Main'

reactMountElements({
  'control-plane': Main
})
